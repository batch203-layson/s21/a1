// console.log("Hello world!");


let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);


function functionOne(addToArrayOne)
{
    users[users.length] = addToArrayOne;
    console.log(users);
}

functionOne("John Cena");



function functionTwo(indexNum)
{
    console.log(users[indexNum]);
}

functionTwo(2);

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function functionThree(returnDeleted)
{
    // let lastElementindex = bullsLegend.length-1;
    returnDeleted = users.length-1;
    console.log(users[returnDeleted]);
    users.length--;
    console.log(users);
}

functionThree();


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/



function functionFour(updateIndex, numOfIndex)
{
    users[numOfIndex]=updateIndex;
    console.log(users);
}

functionFour("Triple h", 3);

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function functionFive()
{
    users=[];
    console.log(users);
}

functionFive();


/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

// let isUserEmpty;
function checkIfEmpty()
{
    if(users.length>0)
    {
        return false;
    }
    else{
        return true;
    }
}
let isUsersEmpty = checkIfEmpty();
console.log(isUsersEmpty)



/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
